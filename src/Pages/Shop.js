import React from 'react';
import CardList from "../components/CardList/CardList";
import Card from "../components/Card/Card";

function Shop(props) {

    const {card, toggleModal} =props
    return (
        <CardList card={card} toggleModal={toggleModal}><Card/></CardList>
    );
}

export default Shop;