import {Routes, Route} from 'react-router-dom'
import Shop from "./Pages/Shop";
import FavoritesPage from "./Pages/FavoritesPage"
import CartPage from "./Pages/CartPage"




function AppRoutes(props) {
    const {card, toggleModal} =props
    return (
       <Routes>
           <Route path={''} element={<Shop card={card} toggleModal={toggleModal}/>}/>
           <Route path={'/cart'} element={<FavoritesPage/>}/>
           <Route path={'/favorites'} element={<CartPage/>}/>
       </Routes>
    );
}

export default AppRoutes;