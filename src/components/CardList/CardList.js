import React from 'react';
import styles from './CartList.module.scss';
import Card from "../Card/Card";
import {PropTypes} from "prop-types"




function CardList(props) {


        const {card} =props;
        const{toggleModal} =props

        return (

            <ul className={styles.items}>
                {card.map(({name,  article, img,price, color})=>{
                   return <Card  toggleModal={toggleModal}  key={article} name={name} img={img}  article={article} price={price} color={color}/>

                })}
            </ul>
        );

}

CardList.propTypes ={
    card:PropTypes.array,

    toggleModal:PropTypes.func,


}



export default CardList;