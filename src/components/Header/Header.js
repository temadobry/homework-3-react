import React from 'react';
import styles from "./Header.module.scss"
import CartCVG from "../CartSVG/CartCVG";
import FavoriteCVG from "../FavoriteSVG/FavoriteCVG";
import {PropTypes} from "prop-types"
import Navigation from "../Navigation/Navigation";

function Header(props) {

        const {counterItems} =props
        return (
            <div className={styles.top}>
                <h2>    <CartCVG/>   {counterItems}</h2>
                <Navigation></Navigation>
                <h2>    <FavoriteCVG/>   {counterItems}</h2>
            </div>
        );

}


Header.propTypes ={
    counterItems:PropTypes.number

}

Header.defaultProps = {
    counterItems: 0,
}


export default Header;