import React from "react"
import styles from  './Modal.module.scss'
import {PropTypes} from "prop-types"

import CrossCVG from "../CrossSVG/CrossCVG";



function Modal(props) {
    const {header,  text, action, toggleModal} =props
    return (
        <div className={styles.root}>
            <div onClick={() => toggleModal(false)} className={styles.background}/>

            <div className={styles.content}>
                <div className={styles.modal_head}>
                    <h2>{header}</h2>
                    <div onClick={()=>{toggleModal(false)}}  >
                        <CrossCVG></CrossCVG>
                    </div>
                </div>

                <h2>{text}</h2>
                {action}
            </div>

        </div>
    );
}





Modal.propTypes ={
    header:PropTypes.string,
    text:PropTypes.string,
    action:PropTypes.node,
    toggleModal:PropTypes.func

}

Modal.defaultProps = {

}

export default Modal
