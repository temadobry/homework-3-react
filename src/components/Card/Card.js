import React from 'react';
import styles from "./Card.module.scss"
import Button from "../Button/Button";
import {PropTypes} from "prop-types"
import FavoriteCVG from "../FavoriteSVG/FavoriteCVG";




function Card(props) {



        const {name,  article, img,price, color,  }=props
        const {toggleModal}=props




        return (
            <div className={styles.card}>
                <div className={styles.card_top}>
                <FavoriteCVG></FavoriteCVG>
                <span className={styles.description}>{article}</span>
                </div>


                <span className={styles.title}>{name}</span>
                <img className={styles.itemAvatar} src={img}
                     alt={name}/>
                <span className={styles.price}>{price} Euro</span>

                <span className={styles.description}>{color}</span>

                <div className={styles.btnContainer}>
                    <Button text="add to cart" onClick={()=>{toggleModal(true)}}></Button>
                </div>
            </div>
        );

}

Card.propTypes ={
    name:PropTypes.string,
    article:PropTypes.number,
    img:PropTypes.string,
    price:PropTypes.number,
    toggleModal:PropTypes.func,


}

Card.defaultProps = {
    img: "https://dogtime.com/assets/uploads/gallery/jack-russel-terrier-dog-breed-pictures/2-face.jpg"

}

export default Card;