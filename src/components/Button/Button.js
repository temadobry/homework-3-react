import React from "react";
import styles from  './Button.module.scss'
import {PropTypes} from "prop-types"


function Button (props){


        const {backgroundColor, text, onClick} =props

        return (

            <button className={styles.btn} onClick={onClick} style={{backgroundColor: backgroundColor}} >{text}</button>

        )

}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func
}

Button.defaultProps = {
    text: "BUTTON"
}

export default Button;
