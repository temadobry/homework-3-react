import {NavLink} from  'react-router-dom'

const Navigation = () => {

    return (
        <nav>
            <ul>
                <li>
                    <NavLink to={"/"}>Shop</NavLink>
                </li>
                <li>
                    <NavLink to={"/cart"}>Cart</NavLink>
                </li>
                <li>
                    <NavLink to={"/favorites"}>Favorites</NavLink>
                </li>
            </ul>
        </nav>

    )

}

export default Navigation