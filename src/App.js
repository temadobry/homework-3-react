import React, {useState, useEffect} from 'react';
import styles from './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import Card from "./components/Card/Card";
import CardList from "./components/CardList/CardList";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";


function App(props) {

    const [firstModalStatus, setFirstModalStatus] = useState(false)
    const [card, setCard] = useState([])
    const [counterItems, setCounterItems] = useState(0)

    const getItems = async () => {
        const res = await fetch("shop.json");
        const card = await res.json();
        setCard(card.data)



    }

    const toggleModal = (value) => {
        setFirstModalStatus(value)

    }

    const increaseCounterItem = () => {
        setCounterItems(counterItems + 1)
        localStorage.setItem("counterItems", JSON.stringify(counterItems + 1))


    }


    useEffect(() => {
        getItems()

    }, [])

    return (
        <div className={styles.App}>

            <Header counterItems={counterItems}></Header>


            <main>
                <AppRoutes card={card} toggleModal={toggleModal}></AppRoutes>

             {/*<CardList card={card} toggleModal={toggleModal}><Card/></CardList>*/}
            </main>
            {
                firstModalStatus &&
                <Modal
                    incrementCartItem={increaseCounterItem}

                    toggleModal={toggleModal}
                    header={"Do you wanna add this item?             "}
                    closeButton={true}
                    text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, veniam.'}
                    action={
                        <div>
                            <Button onClick={() => {
                                increaseCounterItem()
                            }} text="YES" backgroundColor="green"></Button>
                            <Button onClick={() => {
                                toggleModal(false)
                            }} text="No" backgroundColor="red"></Button>
                        </div>
                    }
                >
                </Modal>


            }


        </div>
    );
}

export default App;


// class App extends React.Component {
//     state = {
//         firstModalStatus: false,
//         secondModalStatus: false,
//         card: [],
//         counterItems: 0
//     }
//
//     async componentDidMount() {
//         const {data} = await fetch("shop.json").then(res => res.json())
//         this.setState({card: data})
//         const counterItems = localStorage.getItem("counterItems")
//         this.setState({counterItems: JSON.parse(counterItems)})
//
//     }
//
//     toggleModal = (value) => {
//         this.setState({firstModalStatus: value})
//
//     }
//
//     increaseCounterItem = () => {
//         this.setState({counterItems: this.state.counterItems + 1})
//         localStorage.setItem("counterItems", JSON.stringify(this.state.counterItems + 1))
//         console.log(this.state.counterItems)
//
//     }
//
//
//     render() {
//
//
//         const {firstModalStatus,  card, counterItems} = this.state
//
//
//         return (
//
//             <div className={styles.App}>
//
//                 <Header counterItems={counterItems}></Header>
//
//
//                 <main>
//
//                     <CardList card={card} toggleModal={this.toggleModal}><Card/></CardList>
//
//                 </main>
//                 {
//                     firstModalStatus &&
//                     <Modal
//                         incrementCartItem={this.incrementCartItem}
//
//                         toggleModal={this.toggleModal}
//                         header={"Do you wanna add this item?             "}
//                         closeButton={true}
//                         text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, veniam.'}
//                         action={
//                             <div>
//                                 <Button onClick={() => {
//                                     this.increaseCounterItem()
//                                 }} text="YES" backgroundColor="green"></Button>
//                                 <Button onClick={() => {
//                                     this.toggleModal(false)
//                                 }} text="No" backgroundColor="red"></Button>
//                             </div>
//                         }
//                     >
//                     </Modal>
//
//
//                 }
//
//
//             </div>
//         );
//
//     }
//
// }
//
//
// export default App;
